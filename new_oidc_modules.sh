#!/bin/bash
if [ x"" = x$1 ] || [ x"" = x$2 ]
then
        echo params error
        exit
fi
src_path=../plugins-personal-openid-connect/*
dest_path=.
target_name=$1
target_name2=$2
cp -r $src_path .

find $dest_path -type f | grep -v .sh | xargs sed -i "s/personal/$target_name/g"
find $dest_path -type f | grep -v .sh | xargs sed -i "s/Personal/$target_name2/g"
